## README

Link: https://homework.adhoc.team/noclist

# Getting started

The project includes a `main.rb` script as an entrypoint and some preparations are required before run it:

- Ensure the noclist server is up and running.
```bash
docker run --rm -p 8888:8888 adhocteam/noclist
```

- Ensure `main.rb` script has execution permissions.
```bash
chmod +x ./main.rb
```

Then run the `main.rb` script:
```bash
./main.rb
```

# Tests

Run bundle install to install dependencies and `rspec`.
```bash
bundle install
```

And then run `rspec` to execute the tests suite:
```
bundle exec rspec
```
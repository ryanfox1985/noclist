class NoclistClient
  class ServerSideError < StandardError; end

  require 'digest'
  require 'uri'
  require 'net/http'

  attr_accessor :auth_token

  def auth
    uri = URI("#{endpoint}/auth")
    response = get(uri)

    self.auth_token = response['Badsec-Authentication-Token'] if response.code.match(/200/)

    response
  end

  def users
    uri = URI("#{endpoint}/users")
    get(uri, headers: { 'X-Request-Checksum' => request_checksum('/users') })
  end

  private

  def request_checksum(request_path)
    Digest::SHA256.hexdigest(auth_token + request_path)
  end

  def get(uri, headers: {})
    http_request = Net::HTTP::Get.new(uri)
    headers.each do |k, v|
      http_request[k] = v
    end

    request(http_request)
  end

  def request(http_request)
    attempts ||= 0

    response = Net::HTTP.start(http_request.uri.hostname, http_request.uri.port, read_timeout: 5) do |http|
      http.request(http_request)
    end
    raise ServerSideError if response.code.match(/5[0-9]+/) && attempts < 2

    response
  rescue Net::ReadTimeout, ServerSideError
    raise if (attempts += 1) > 2

    warn("Timeout for uri: #{http_request.uri}, attempts: (#{attempts})")
    retry
  end

  def endpoint
    ENV.fetch('NOCLIST_ENDPOINT_URL', 'http://localhost:8888')
  end
end

require './noclist_client'
require 'net/http'

describe NoclistClient do
  let(:instance) { described_class.new }
  let(:token) { 'token' }
  let(:uri) { URI('http://hello.com/users') }

  describe '#auth' do
    subject { instance.auth }

    context 'when http success' do
      let(:http_response) { instance_double(Net::HTTPOK, code: '200') }

      before do
        allow(instance).to receive(:get).and_return(http_response)
        allow(http_response).to receive(:[]).with('Badsec-Authentication-Token').and_return(token)
      end

      it 'set token from header' do
        subject
        expect(instance.auth_token).to eq(token)
      end
    end

    context 'when http no success' do
      let(:http_response) { instance_double(Net::HTTPBadRequest, code: '400') }

      before do
        allow(instance).to receive(:get).and_return(http_response)
      end

      it 'not set token' do
        subject
        expect(instance.auth_token).to eq(nil)
      end
    end
  end

  describe '#users' do
    subject { instance.users }

    context 'with a valid token' do
      before do
        instance.auth_token = token
      end

      it 'calculate header checksum' do
        allow(instance).to receive(:get)
        subject
        expect(instance).to have_received(:get).with(
          anything,
          headers: { 'X-Request-Checksum' => Digest::SHA256.hexdigest("#{token}/users") }
        )
      end
    end
  end

  describe '#get' do
    subject { instance.send(:get, uri) }

    let(:http_response) { instance_double(Net::HTTPOK, code: '200') }

    before do
      allow(instance).to receive(:get).and_return(http_response)
    end

    it 'return the response' do
      expect(subject).to eq(http_response)
    end
  end

  describe '#request' do
    subject { instance.send(:request, Net::HTTP::Get.new(uri)) }

    let(:http) { instance_double(Net::HTTP) }

    context 'when http success' do
      let(:http_response) { instance_double(Net::HTTPOK, code: '200') }

      before do
        allow(Net::HTTP).to receive(:start).and_yield(http)
        allow(http).to receive(:request).and_return(http_response)
      end

      it 'return the success response' do
        expect(subject).to eq(http_response)
      end
    end

    context 'when timeout' do
      before do
        allow(Net::HTTP).to receive(:start).and_raise(Net::ReadTimeout)
      end

      it 'retry three times' do
        expect(Net::HTTP).to receive(:start).thrice.times
        expect { subject }.to raise_error(Net::ReadTimeout)
      end
    end

    context 'when server side error' do
      let(:http_response) { instance_double(Net::HTTPInternalServerError, code: '500') }

      before do
        allow(Net::HTTP).to receive(:start).and_yield(http)
        allow(http).to receive(:request).and_return(http_response)
      end

      it 'retry three times' do
        expect(Net::HTTP).to receive(:start).thrice.times
        expect(subject).to eq(http_response)
      end
    end
  end
end

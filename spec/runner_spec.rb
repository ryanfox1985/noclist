require './runner'

describe Runner do
  let(:http_client) { instance_double(NoclistClient) }

  before do
    allow(NoclistClient).to receive(:new).and_return(http_client)
  end

  describe '#run' do
    subject { described_class.run }

    context 'when everything works' do
      let(:http_auth_response) { instance_double(Net::HTTPOK, code: '200') }
      let(:http_users_response) { instance_double(Net::HTTPOK, code: '200', body: "XXXXX\nYYYYY") }

      before do
        allow(http_client).to receive(:auth).and_return(http_auth_response)
        allow(http_client).to receive(:users).and_return(http_users_response)
      end

      it 'not raises an error' do
        expect { described_class.run }.to output("[\"XXXXX\", \"YYYYY\"]\n").to_stdout
      end
    end

    context 'when unexpected response from login' do
      let(:http_auth_response) { instance_double(Net::HTTPBadRequest, code: '400', body: 'Bad Request') }

      before do
        allow(http_client).to receive(:auth).and_return(http_auth_response)
      end

      it 'raises an exit error' do
        expect { described_class.run }.to raise_error(SystemExit)
      end
    end

    context 'when unexpected response from users' do
      let(:http_auth_response) { instance_double(Net::HTTPOK, code: '200') }
      let(:http_users_response) { instance_double(Net::HTTPBadRequest, code: '400', body: 'Bad Request') }

      before do
        allow(http_client).to receive(:auth).and_return(http_auth_response)
        allow(http_client).to receive(:users).and_return(http_users_response)
      end

      it 'raises an exit error' do
        expect { described_class.run }.to raise_error(SystemExit)
      end
    end

    context 'when timeout' do
      before do
        allow(http_client).to receive(:auth).and_raise(Net::ReadTimeout)
      end

      it 'raises an exit error' do
        expect { described_class.run }.to raise_error(SystemExit)
      end
    end
  end
end

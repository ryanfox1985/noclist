require './noclist_client'

class Runner
  def self.run
    http_client = NoclistClient.new
    auth_response = http_client.auth

    unless auth_response.code.match(/200/)
      warn("Unexpected response from /auth: code: #{auth_response.code}, body: #{auth_response.body}")
      exit 1
    end

    users_response = http_client.users
    unless users_response.code.match(/200/)
      warn("Unexpected response from /users: code: #{users_response.code}, body: #{users_response.body}")
      exit 1
    end

    p users_response.body.split("\n")
  rescue Net::ReadTimeout, Errno::ECONNREFUSED, NoclistClient::ServerSideError
    exit 2
  end
end
